import express from "express";
import fileUpload from "express-fileupload";
import cors from "cors";
import { createServer } from "http";
import { Server } from "socket.io";
import dotenv from "dotenv";
dotenv.config();

import path from "path";
const __dirname = path.resolve();

const app = express();
const server = createServer(app);
const io = new Server(server);

// parser
app.use(express.json());
app.use(fileUpload());
app.use(cors());
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.sendFile("index.html", { root: "./public" });
});
app.get("/chat", (req, res) => {
  res.sendFile("chat-room.html", { root: "./public" });
});

io.on("connection", (socket) => {
  console.log("Socket connected");

  socket.on("join-chat", (e) => {
    let data = JSON.parse(e);
    let roomID = data.roomID;
    let name = data.name;
    // console.log(data);

    socket.join(roomID);

    socket.on(roomID, (e) => {
      let msg_data = {
        name,
        msg: e,
      };
      socket.broadcast.emit(roomID, JSON.stringify(msg_data));
    });

    socket.on("disconnecting", (e) => {
      let msg_data = {
        name: "Robo Server",
        msg: `${name} left the chat.`,
      };
      socket.broadcast.emit(roomID, JSON.stringify(msg_data));
    });
  });
});

// listen for requests :)
// port infos
const port = process.env.PORT || 8000;
server.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
