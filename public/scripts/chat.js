let socket = undefined;
let qr_code_element;
let user_name = undefined,
  roomID = undefined;

const uuid = () => {
  const url = URL.createObjectURL(new Blob());
  return url.substring(url.lastIndexOf("/") + 1);
};

window.onresize = () => {
  let input_bar = document.getElementsByClassName("chat-input")[0];
  console.log(input_bar.clientHeight);

  document.getElementsByClassName("msg-holder")[0].style.paddingBottom = `${
    input_bar.clientHeight + 10
  }px`;
  window.scrollTo(0, document.body.scrollHeight);
};

window.onload = () => {
  qr_code_element = document.querySelector(".qr-code");
  document.getElementsByClassName("msg-holder")[0].innerHTML = "";

  document.getElementById("close-qr").onclick = () => {
    document.getElementsByClassName("qr-code-container")[0].style.display =
      "none";
  };

  document.getElementById("qr").onclick = () => {
    document.getElementsByClassName("qr-code-container")[0].style.display =
      "flex";
  };

  let url = new URL(window.location);
  user_name = url.searchParams.get("name");
  roomID = url.searchParams.get("roomID");

  if (user_name == null) {
    user_name = "Anonymous";
  }

  if (roomID == null) {
    roomID = uuid();
    if (window.location.toString().indexOf("?") == -1) return;
    window.location += `&roomID=${roomID}`;
  }
  let link = window.location.origin;
  link += `?roomID=${roomID}`;
  generate({
    value: link,
  });

  console.log(`Name: ${user_name}, roomID: ${roomID}`);

  socket = io();

  socket.emit("join-chat", JSON.stringify({ name: user_name, roomID }));
  socket.on(roomID, (e) => {
    let data = JSON.parse(e);
    add_msg(data);
  });

  document.getElementById("invite").onclick = (e) => {
    navigator.clipboard.writeText(link).then(
      () => {
        alert("link copied to clipboard");
      },
      () => {
        console.error("Failed to copy");
        alert(`link: ${link}`);
      }
    );
  };

  document.addEventListener("keyup", (event) => {
    event.preventDefault();
    if (event.key != "Enter") return;

    document.getElementById("text-send-btn").click();
  });

  document.getElementById("text-send-btn").addEventListener("click", send_msg);
};

const add_msg = (data, is_self = false) => {
  let side_class = "";
  if (is_self) {
    side_class = "right";
  }
  document.getElementsByClassName("msg-holder")[0].innerHTML += `
  <div class="msg ${side_class}">
        <div class="image-holder">
          <img src="https://robohash.org/${data.name}?set=set3" alt="image" />
        </div>
        <div class="text-holder">
          <div class="author">${data.name}</div>
          <div class="text">
            ${data.msg}
          </div>
        </div>
      </div>`;

  window.scrollTo(0, document.body.scrollHeight);
};

const send_msg = () => {
  let msg = document.getElementById("msg-input").value;
  let data = {
    name: user_name,
    msg,
  };
  add_msg(data, true);
  socket.emit(roomID, msg);

  document.getElementById("msg-input").value = "";
};

const generate = (user_input) => {
  qr_code_element.style = "";

  var qrcode = new QRCode(qr_code_element, {
    text: `${user_input.value}`,
    width: 180,
    height: 180,
    colorDark: "#000000",
    colorLight: "#ffffff",
    correctLevel: QRCode.CorrectLevel.H,
  });
};
