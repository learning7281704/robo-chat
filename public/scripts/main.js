window.onload = () => {
  let url = new URL(window.location);
  let roomID = url.searchParams.get("roomID");

  document.getElementById("roomID").value = roomID;
};
